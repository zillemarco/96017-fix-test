var noteBox;

function setupNoteTemplates() {
  var id = document.activeElement.id;
  if (id != 'note-body' && id != 'note_note') {
    noteBox = document.getElementById('note-body')
  } else {
    noteBox = document.activeElement;
  }
  var templates = [];

  if (document.querySelector('[title*="W::Closed"]')) {
    templates.push({
      name: 'Open',
      id: 'bmkl-open',
      callback: function() {
        selectNoteTemplate('\\n/spend m\\n/reopen\\n/label ~workflow::');
      }
    });
  } else {
    templates.push({
      name: 'Close',
      id: 'bmkl-close',
      callback: function() {
        selectNoteTemplate('\\n/spend m\\n/close\\n/label ~workflow::closed');
      }
    });
  }

  if (document.querySelector('[title*="W::Pending"]')) {
    templates.push({
      name: 'Unpend',
      id: 'bmkl-unpend',
      callback: function() {
        selectNoteTemplate('\\n/spend m\\n/remove_due_date\\n/label ~workflow::next');
      }
    });
  } else {
    templates.push({
      name: 'Pend',
      id: 'bmkl-pend',
      callback: function() {
        selectNoteTemplate('\\n/spend m\\n/due in 7 days\\n/label ~workflow::pending');
      }
    });
  }

  var templateBox = noteBox.parentElement.parentElement.lastChild;
  templateBox.innerHTML = '';
  templates.forEach((t) => {
    templateBox.innerHTML += '<button type="button" class="btn" id="' + t.id + '">' + t.name + '</button>';
  });
  
  // wait for the elements to be added
  setTimeout(function() {
    templates.forEach((t) => {
      document.getElementById(t.id).addEventListener("click", t.callback);
    });
  }, 1000);
}

function selectNoteTemplate(text) {
  noteBox.value += text;
}